#!/usr/bin/make -f
# debian/rules file for ircd-hybrid,
# created by Joshua Kwan <joshk@triplehelix.org> for ircd-hybrid-7.0.x,
# modified by Aurélien GÉRÔME for ircd-hybrid-7.2.2.

export CFLAGS = -g

# Make my life easier
I = debian/ircd-hybrid
I_LIB = $(I)/usr/lib/ircd-hybrid
I_SBIN = $(I)/usr/sbin
I_DOC = $(I)/usr/share/doc/ircd-hybrid
I_ETC = $(I)/etc/ircd-hybrid

buildtype := $(shell dpkg-architecture -qDEB_BUILD_GNU_TYPE)
hosttype := $(shell dpkg-architecture -qDEB_HOST_GNU_TYPE)

arch := $(shell dpkg-architecture -qDEB_HOST_ARCH)

package_version = $(shell dpkg-parsechangelog | sed -n 's/^Version: *//p')

ifeq ($(arch),alpha)
	configure_options := --enable-poll
endif

ifeq ($(arch),kfreebsd-i386)
	configure_options := --enable-poll
endif

ifeq ($(arch),kfreebsd-amd64)
	configure_options := --enable-poll
endif

ifeq ($(arch),hurd-i386)
	configure_options := --enable-poll
endif

ifneq ($(buildtype),$(hosttype))
	crossflags := --build=$(buildtype) --host=$(hosttype)
endif

configure: configure-stamp

configure-stamp:
	dh_testdir

	dh_autoreconf

	./configure	$(crossflags) \
			--with-tls=gnutls \
			--prefix=/usr \
			--sysconfdir=/etc/ircd-hybrid \
			--localstatedir=/var \
			$(configure_options) \
			$(shell dpkg-buildflags --export=configure) \
			--program-suffix=-hybrid \
			--without-included-ltdl PACKAGE_VERSION=$(package_version)
	touch $@

build: build-arch build-indep

build-arch: build-stamp

build-indep: build-stamp

build-stamp: configure
	$(MAKE)
	
	touch build-stamp

manpages: manpages-stamp

manpages-stamp:
	mkdir -p debian/doc-pages
	@echo "Processing man pages: "
	@for i in $(wildcard debian/doc/*.sgml); do \
		TARGET="debian/doc-pages/`basename $$i | sed 's/\.sgml//'`"; \
		echo "$$i -> $$TARGET"; \
		docbook-to-man $$i > $$i.tmp; \
		sed -n "`grep -n '^.TH' $$i.tmp | awk -F : '{print $$1}'`~1p" $$i.tmp > $$TARGET; \
		rm -f $$i.tmp; \
	done
	touch $@

clean:
	dh_testroot
	dh_testdir
	rm -rf debian/doc-pages
	rm -f build-stamp configure-stamp manpages-stamp config.log changelog-stamp control-stamp
	[ ! -f Makefile ] || $(MAKE) distclean
	rm -f include/patchlevel-debian.h
	dh_clean

install: manpages

	dh_testdir
	dh_prep
	dh_installdirs -A
	$(MAKE) DESTDIR=$(CURDIR)/debian/tmp install
	rm -f $(CURDIR)/debian/tmp/etc/ircd-hybrid/reference.conf
	install -d $(CURDIR)/debian/tmp/var/cache/ircd-hybrid

	chmod +x $(CURDIR)/debian/scripts/*
	dh_install

	cp debian/cert.cnf $(I_ETC) 

binary-indep: build install

	dh_testdir
	dh_installchangelogs -i
	dh_installdocs -i
	dh_installman -i
	dh_compress -i
	dh_fixperms -i
	dh_installdeb -i
	dh_gencontrol -i
	dh_md5sums -i
	dh_builddeb -i

binary-arch: build install

	dh_testdir
	dh_testroot
	dh_installdocs -a -Xdoc/ircd. -Xline. -XMakefile -Xexample
	dh_installexamples
	dh_installman -a
	dh_installchangelogs -a
	dh_compress -a
	dh_lintian

	# mkpasswd -Hmd5
	rm -f $(I_SBIN)/mkpasswd
	rm -f $(I_ETC)/.convertconf-example.conf $(I_ETC)/example*

	# Fixes lintian error non-empty-dependency_libs-in-la-file
	sed -i "/dependency_libs/ s/'.*'/''/" `find . -name '*.la'`

	dh_fixperms -a

	chown irc:irc debian/ircd-hybrid/var/log/ircd
	chown root:irc $(I_ETC)
	chmod 750 $(I_ETC)
	chown root:irc $(I_ETC)/ircd.conf
	chmod 640 $(I_ETC)/ircd.conf
	chown irc:irc debian/ircd-hybrid/var/cache/ircd-hybrid
	chmod 700 debian/ircd-hybrid/var/cache/ircd-hybrid

	dh_installinit -a -n
	dh_installdebconf -a
	dh_installlogrotate -a

	dh_strip -a
	dh_link -a
	dh_installdeb -a
	dh_shlibdeps -a
	dh_gencontrol -a
	dh_md5sums -a
	dh_builddeb -a

binary:	binary-indep binary-arch

.PHONY: binary binary-arch binary-indep clean install manpages
