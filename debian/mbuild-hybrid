#!/bin/bash
# Shell script to assist with building ircd-hybrid modules and installing
# them into the correct dirs so they can be modloaded.
# (c) 2003 by Joshua Kwan. Licensed under the GNU GPL,
# see /usr/share/common-licenses/GPL on Debian systems.
#
# Syntax: mbuild-hybrid [-s m_modsource.c] [-p outputpath] [-o misc-cflags]

OUTPATH=/usr/lib/ircd-hybrid/modules
CFLAGS="-O2 -g -Wall "
until [ -z "$1" ]
do
	case "$1" in
		-s) MODSOURCE=$2; shift; shift;;
		-p) OUTPATH=$2; shift; shift;;
		-o) CFLAGS+=$2; shift; shift;;
		--help)
			echo "usage: mbuild-hybrid [-s m_modsource.c] [-p outputpath] [-o misc-cflags]"
			exit 0
		;;
	esac
done	

if [ -z $MODSOURCE ]; then
	echo "error: no source file specified"
	exit 1
fi

if [ ! -d "/usr/include/ircd-hybrid-8" ]; then
	echo "error: where are the hybrid includes? did you delete them?"
	exit 1
fi

MODSOURCE_SO=`echo $MODSOURCE | sed 's/\.c/\.so/'`
echo "Module source file: $MODSOURCE"
echo "Install path: $OUTPATH"
echo "Additional compile flags: $CFLAGS"
echo "--> Compiling: gcc -I/usr/include/ircd-hybrid-8 -fPIC -DPIC -shared $CFLAGS $MODSOURCE -o $MODSOURCE_SO"
gcc -I/usr/include/ircd-hybrid-8 -fPIC -DPIC -shared $CFLAGS $MODSOURCE -o $MODSOURCE_SO || BUILD_FAILED=1

if [ "$BUILD_FAILED" = "1" ]; then echo "build FAILED, exiting!"; exit 1; fi

echo "--> Installing into $OUTPATH"
mv $MODSOURCE_SO $OUTPATH || INSTALL_FAILED=1

if [ "$INSTALL_FAILED" = "1" ]; then echo "install FAILED, exiting"; exit 1; fi

echo "--> Done!"
